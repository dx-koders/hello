LANGUAGES

You are free to use any language you can find; however keep in mind that if you (or the person reviewing your code) cannot run it then you likely will not get any points.

### List of Languages

- [05AB1E](https://hub.docker.com/r/esolang/05ab1e) - difficult
- [ABC](https://hub.docker.com/r/esolang/abc) - easy?
- [ALGOL](https://en.wikipedia.org/wiki/ALGOL)
- [ActionScript3](https://apache.github.io/royale-docs/get-started) - easy?
- [apl](https://hub.docker.com/r/esolang/apl)
- [assembly](https://hub.docker.com/r/esolang/x86asm-nasm) - difficult
- [awk](https://hub.docker.com/r/esolang/awk)
- [backhand](https://hub.docker.com/r/esolang/backhand) - difficult
- [ballerina](https://hub.docker.com/r/esolang/ballerina) - moderate
- [bash](https://savannah.gnu.org/projects/bash/) - easy
- [BASIC](https://hub.docker.com/r/primeimages/freebasic) - easy
- [bots`](https://hub.docker.com/r/esolang/bots) - difficult
- [C](https://hub.docker.com/r/esolang/c-gcc) - moderate
- [C#](https://hub.docker.com/r/esolang/csharp) - moderate
- [C++](https://hub.docker.com/r/esolang/cpp-clang) - moderate
- [Cardinal](https://hub.docker.com/r/esolang/cardinal)
- [Clojure](https://hub.docker.com/_/clojure) - moderate
- [COBOL](https://hub.docker.com/r/esolang/cobol) - moderate
- [Cogent](https://cogent.readthedocs.io/en/latest/)
- [CoffeeScript](https://hub.docker.com/r/codesignal/coffeescript)
- [Common LISP (SBCL)](https://hub.docker.com/r/esolang/clisp-sbcl) - moderate
- [Crystal](https://hub.docker.com/r/esolang/crystal) - easy?
- [D](https://hub.docker.com/r/esolang/d-gdc) - moderate
- [Dafny](https://github.com/dafny-lang/dafny)
- [Dart](https://hub.docker.com/_/dart)
- [Egison](https://www.egison.org/) - moderate
- [Element](https://hub.docker.com/r/esolang/element)
- [Elixir](https://hub.docker.com/r/esolang/elixir)
- [Elm](https://elm-lang.org/)
- [Erlang](https://hub.docker.com/r/esolang/erlang) - moderate
- [evil](https://hub.docker.com/r/esolang/evil)
- [F#](https://hub.docker.com/r/esolang/fsharp-dotnet)
- [F*](https://www.fstar-lang.org/)
- [Fortran](https://hub.docker.com/r/esolang/fortran)
- [gaia](https://hub.docker.com/r/esolang/gaia)
- [gnuplot](https://hub.docker.com/r/esolang/gnuplot) - moderate?
- [Go](https://hub.docker.com/r/esolang/golang) - moderate
- [Groovy](https://hub.docker.com/_/groovy)
- [Haskell](https://hub.docker.com/r/esolang/haskell) - moderate
- [HTML](https://www.w3.org/html/) - easy
- [husk](https://hub.docker.com/r/esolang/husk) - difficult
- [hypertorus](https://hub.docker.com/r/esolang/hypertorus)
- [IO](https://iolanguage.org/)
- [Java](https://hub.docker.com/r/esolang/java) - moderate
- [Java 17](https://www.oracle.com/news/announcement/oracle-releases-java-17-2021-09-14/) - moderate
- [JavaScript](https://hub.docker.com/r/esolang/js-rhino) - easy
- [Jelly](https://hub.docker.com/r/esolang/jelly)
- [Jellyfish](https://hub.docker.com/r/esolang/jellyfish)
- [Julia](https://hub.docker.com/_/julia)
- [Kind](https://github.com/HigherOrderCO/Kind) - difficult
- [Koka](https://koka-lang.github.io/koka/doc/index.html)
- [Kotlin](https://hub.docker.com/r/esolang/kotlin)
- [Links](https://links-lang.org/)
- [Lobster](https://github.com/aardappel/lobster)
- [Lua](https://hub.docker.com/r/esolang/lua)
- [m4](https://hub.docker.com/r/esolang/m4)
- [MATL](https://hub.docker.com/r/esolang/matl)
- [Matlab](https://hub.docker.com/r/mathworks/matlab)
- [mines](https://hub.docker.com/r/esolang/mines)
- [minus](https://hub.docker.com/r/esolang/minus) - difficult
- [moo](https://hub.docker.com/r/esolang/moo) - difficult
- [nim](https://nim-lang.org/)
- [Objective-C](http://developer.apple.com/library/mac/navigation)
- [ocaml](https://hub.docker.com/r/esolang/ocaml)
- [Pascal](https://en.wikipedia.org/wiki/Pascal_(programming_language))
- [Perl](https://hub.docker.com/_/perl) - easy
- [PHP](https://hub.docker.com/_/php) - easy
- [Pony](https://www.ponylang.io/)
- [PowerShell](https://hub.docker.com/r/esolang/powershell) - easy
- [PureScript](https://www.purescript.org/)
- [Python](https://hub.docker.com/_/python) - easy
- [QB64](https://qb64.com/) - easy
- [R](https://www.r-project.org/)
- [Racket](https://hub.docker.com/r/esolang/racket)
- [REBOL](http://www.rebol.com/)
- [RED](https://www.red-lang.org/)
- [RPG](https://en.wikipedia.org/wiki/IBM_RPG)
- [rprogn](https://hub.docker.com/r/esolang/rprogn)
- [Ruby](https://hub.docker.com/_/ruby) - easy
- [Rust](https://hub.docker.com/r/esolang/rust) - moderate
- [Scala](https://www.scala-lang.org/)
- [Scheme](http://www.scheme-reports.org/)
- [Scratch](https://scratch.mit.edu/)
- [sed](https://hub.docker.com/r/esolang/sed)
- [Self](https://selflanguage.org/)
- [SmalBASIC](https://sourceforge.net/projects/smallbasic/) - easy
- [Smalltalk](https://en.wikipedia.org/wiki/Smalltalk)
- [SNOBOL](https://hub.docker.com/r/esolang/snobol)
- [Swift](https://hub.docker.com/r/esolang/swift) - moderate
- [Tcl](https://hub.docker.com/r/esolang/tcl)
- [TeX](https://hub.docker.com/r/esolang/tex) - easy?
- [triangularity](https://hub.docker.com/r/esolang/triangularity) - difficult
- [TypeScript](https://www.typescriptlang.org/)
- [V](https://hub.docker.com/r/esolang/vlang)
- [Vala](https://wiki.gnome.org/Projects/Vala)
- [VBScript](https://stackoverflow.com/questions/20335878/is-it-possible-to-run-a-vbscript-in-unix-environment) - easy?
- [VisualBasic](https://en.wikipedia.org/wiki/Visual_Basic_(.NET))
- [WebAssembly](https://webassembly.org/) - moderate
- [whitespace](https://hub.docker.com/r/esolang/whitespace) - difficult
- [wren](https://wren.io/) - easy
- [Zig](https://ziglang.org/)
- [Zucchini](https://hub.docker.com/r/esolang/zucchini)



### Links

- [comprehensive list](https://en.wikipedia.org/wiki/List_of_programming_languages)

