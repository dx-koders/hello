# README

### About

This project is for the 'Hello' challenge - a challenge to say 'Hello' in as many programming languages as possible.


### Objective

The objective of this project is to put basic programming techniques and knowledge to use in an easy, yet practical, manner.


### Rules

Your program must perform the following:

- prompt user to enter their name and then print out "Hello, {name}!"
- use a loop to print Hello or Goodbye for each number between 1 and 10 (inclusive), for example:
```
    - 1 Goodbye
    - 2 Hello
    - 3 Goodbye
    - 4 Hello
    - 5 Goodbye
    - 6 Hello
    - 7 Goodbye
    - 8 Hello
    - 9 Goodbye
    - 10 Hello
```
- must run without errors

To get credit for a language, you must commit and push the code to your branch of this project.


### Tips

- less code is better than more
- easily understood code is better than complex
- earn 2 bonus points if your program is dockerized
- feel free to look at code from other people or search the internet for examples but you must give proper attribution; also please do not copy/paste code - retype it at the least or better yet add your own twist to it!
- see the `_examples` directory for two complete examples!
- see the `LANGUAGES.md` file for a list of languages - if you know of or can find other languages feel free to use them and please add to the list


### Do Not Forget

- to include any/all info needed to get your code to run (usually in the README.md)
- to include a LICENSE


### Project Structure

root
  + language1
    + all files for language1
  + language2
    + all files for language2


### Git Structure

- master
  + username1
  + username2

You can see the `drad` branch for an example.


### Getting Started

You can get started by:

- cloning the repo: `git@gitlab.com:dx-koders/hello.git`
- create your branch: `git checkout -b {username}`
- make your changes
- add your changes to git: `git add --all`
- commit your changes: `git commit -m '<describe your changes>'
  + replace <describe your changes> with what you did, as an example: `git commit -m 'completed hello in python and cobol'`
- push your branch: `git push`
- you can now submit points for 'hello code challenge 1 language' your work (and dont forget to also submit 'hello code challenge 1 language (bonus)' if you did the docker piece 


### Help

If you need help contact drad or submit a project issue ticket.
