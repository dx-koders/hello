# README for _example

### NOTICES

- we use the TRIM function (GnuCOBOL) which is enabled by `-fintrinsics=TRIM` in the compile of the program

### Setup

- you can run this locally by installing [GnuCobol](https://gnucobol.sourceforge.io/); however we recommend simply using the docker method below

#### Docker

- build: `docker build -t hello-cobol .`


### Running


#### Docker

- execute: `docker run --rm -it hello-cobol`
    + note: you can run direct (no build) with: `docker run --rm -v "$PWD":/app:ro esolang/cobol cobol /app/hello.cbl` but this has issues/limitations
        - limitation: no useful compile error/warning messages
        - issue: does not prompt for user input (ACCEPT)


### Code Tips

- spacing matters in cobol, your `IDENTIFICATION DIVISION.` must start in column 7
- you end a line with a period (.)

### Links

- [Brief intro to COBOL User Input](https://www.linkedin.com/learning/cobol-essential-training/obtain-input-from-the-user)
- [Gnu COBOL Manual](https://gnucobol.sourceforge.io/doc/gnucobol.html)
