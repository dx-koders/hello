       IDENTIFICATION DIVISION.
       PROGRAM-ID. HELLO.
       AUTHOR. DRAD.
       ENVIRONMENT DIVISION.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 WS-AUTH    PIC A(4) VALUE "drad".
       01 WS-CREATED PIC X(10) VALUE "2023-10-01".
       01 WS-VER     PIC X(5) VALUE "0.1.0".
       01 WS-SEP     PIC X(33) VALUE "--------------------------------".
       01 WS-NAME    PIC X(50).
       01 WS-X       PIC 9(2) VALUE 1.

       PROCEDURE DIVISION.
       P1.
           DISPLAY "What is your name? ".
           ACCEPT WS-NAME.
           DISPLAY " "
           DISPLAY WS-SEP
           DISPLAY "Hello v", WS-VER, " - ", WS-AUTH " (" WS-CREATED ")"
           DISPLAY WS-SEP
           *> NOTICE: we use the GnuCOBOL TRIM funcion enabled by `-fintrinsics=ALL` in compile
           DISPLAY "Hello, ", TRIM(WS-NAME), "!".
           DISPLAY "Greeting by odd/even; even=Hello, odd=Goodbye".
       P2.
           IF FUNCTION MOD (WS-X, 2) = 0
               DISPLAY "- ", WS-X, " Hello"
           ELSE
               DISPLAY "- ", WS-X, " Goodbye"
           END-IF.
           COMPUTE WS-X = WS-X + 1.
       P3.
           PERFORM P2 9 TIMES.
       STOP RUN.
