# README for _example

### Setup

- install python (3+)
- ensure script is executable: `chmod u+x hello.py`

#### Docker

- build: `docker build -t hello-python .`


### Running

- execute: `./hello.py`

#### Docker

- execute: `docker run --rm -it hello-python`
