#! /usr/bin/env python3

# @author: drad
# @description: hello in python

_author = "drad"
_created = "2023-09-29"
_version = "0.1.0"

name = input("What is your name? ")

print(f"""
---------------------------------
Hello v{_version} - {_author} ({_created})
---------------------------------
Hello, {name}!
Greeting by odd/even; even=Hello, odd=Goodbye""")

for i in range(1,11):
    if (i % 2) == 0:
        print(f"- {i} Hello")
    else:
        print(f"- {i} Goodbye")

print("Using inline if...")
for i in range(1,11):
    print(f"- {i} Hello" if (i % 2) == 0 else f"- {i} Goodbye")

print("Using inline if more efficiently...")
for i in range(1,11):
    print(f"- {i} {'Hello' if (i % 2) == 0 else f'Goodbye'}")
